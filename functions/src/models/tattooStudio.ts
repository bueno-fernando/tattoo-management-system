import {Base, Document} from "./base"

export type TattooStudioDocument = Document & {
    id?: string
    name: string
    address?: string
}

export class TattooStudio extends Base {

    fields: TattooStudioDocument;
    constructor() {
        super("tattoStudios");

        this.fields = {
            name: "",
        }
    }

    async create() {
        this.id = this.fields.id;
        return await super.save();
    }

    async save() {
        return await super.save();
    }
}