import {Base, Document} from "./base"

export type OrderDocument = Document & {
    id?: string
    tattooStudioId: string
    userId: string
    customerId: string
    scheduleId?: string
    status: OrderStatus
    price?: number
}

export enum OrderStatus {
    NEW = "new",
    BUDGETING = "budgeting",
    BUDGET_SENT = "budget sent",
    BUDGET_APPROVED = "budget approved",
    NEGOTIATION = "negotiation",
    BUDGET_REPROVED = "budget reproved",
    SCHEDULING = "scheduling",
    SCHEDULED = "scheduled",
    FINISHED = "fineshed",
    CANCELED = "canceled"
}

export class Order extends Base {

    fields: OrderDocument;
    constructor() {
        super("orders");

        this.fields = {
            tattooStudioId: "",
            userId: "",
            customerId: "",
            status: OrderStatus.NEW
        }
    }

    async create() {
        this.id = this.fields.id;
        return await super.save();
    }

    async save() {
        return await super.save();
    }
}