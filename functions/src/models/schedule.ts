import {Base, Document} from "./base"
import { firestore } from "firebase-admin"

export type ScheduleDocument = Document & {
    id?: string
    userId: string
    schedule: firestore.Timestamp
    status: ScheduleStatus
}

export enum ScheduleStatus {
    OPEN = "open",
    SCHEDULED = "scheduled",
    BLOCKED = "blocked"
}

export class Schedule extends Base {

    fields: ScheduleDocument;
    constructor() {
        super("schedules");

        this.fields = {
            userId: "",
            schedule: firestore.Timestamp.now(),
            status: ScheduleStatus.OPEN
        }
    }

    async create() {
        this.id = this.fields.id;
        return await super.save();
    }

    async save() {
        return await super.save();
    }
}