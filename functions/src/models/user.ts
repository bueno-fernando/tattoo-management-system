import {Base, Document} from "./base"

export type UserDocument = Document & {
    id?: string
    tattooStudioId: string
    type: UserType
    name?: string
    email: string
    password: string
}

export enum UserType {
    TATTOO_MAKER = "tattoo maker",
    ASSISTENT = "assistent"
}

export class User extends Base {

    fields: UserDocument;
    constructor() {
        super("users");

        this.fields = {
            tattooStudioId: "",
            email: "",
            password: "",
            type: UserType.TATTOO_MAKER
        }
    }

    async create() {
        this.id = this.fields.id;
        return await super.save();
    }

    async save() {
        return await super.save();
    }
}