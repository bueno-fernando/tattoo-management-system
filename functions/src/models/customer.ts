import {Base, Document} from "./base"

export type CustomerDocument = Document & {
    id?: string
    name: string
    email: string
    password?: string
    phone?: string
    cpf?: string
}

export class Customer extends Base {

    fields: CustomerDocument;
    constructor() {
        super("customers");

        this.fields = {
            name: "",
            email:""
        }
    }

    async create() {
        this.id = this.fields.id;
        return await super.save();
    }

    async save() {
        return await super.save();
    }
}