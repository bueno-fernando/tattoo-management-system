import { Request, Response } from "express"
import { TattooStudio, TattooStudioDocument } from "../models/tattooStudio"
import * as moment from 'moment-timezone'
import { toMoment } from "../utils/date"

export class TattooStudioController{
    
    async route_postJSON(req: Request, res: Response) {
        try {
            const doc = req.body as TattooStudioDocument
            const tattooStudio = new TattooStudio()
            Object.assign(tattooStudio.fields, doc)
            if (!await tattooStudio.create()) return res.sendStatus(422)
            tattooStudio.fields.id = tattooStudio.id
            tattooStudio.fields = toMoment(tattooStudio.fields)
            return res.json({
                "tattooStudio": tattooStudio.fields,
                "now": moment().tz('America/Sao_Paulo').format()
            })
        } catch (error) {
            return res.json({
                error
            })
        }
    }

    async route_getJSON(req: Request, res: Response) {
        const tattooStudioId = req.params.tattooStudioId
        if (!tattooStudioId) return res.status(400).send({"response": "tattooStudioId is necessary"})
        const tattooStudio = new TattooStudio()
        if (!await tattooStudio.load(tattooStudioId)) return res.status(400).send({"response": "Tattoo Studio not found", tattooStudioId: tattooStudioId})
        tattooStudio.fields.id = tattooStudio.id
        tattooStudio.fields = toMoment(tattooStudio.fields)
        return res.json({
            "tattooStudio": tattooStudio.fields, 
            "now": moment().tz('America/Sao_Paulo').format()
        })
    }

    async route_deleteJSON(req: Request, res: Response) {
        const tattooStudioId = req.params.tattooStudioId
        if (!tattooStudioId) return res.status(400).send({"response": "tattooStudioId is necessary"})
        const tattooStudio = new TattooStudio()
        if (!await tattooStudio.delete(tattooStudioId)) return res.status(400).send({"response": "Tattoo Studio not found", tattooStudioId: tattooStudioId})
        return res.status(200).send({"response": "Tattoo Studio successfully deleted"})
    }

    async route_putJSON(req: Request, res: Response) {
        try {
            const doc = req.body as TattooStudioDocument
            const tattooStudioId = req.params.orderId
            const tattooStudio = new TattooStudio()
            if(!await tattooStudio.load(tattooStudioId)) return res.status(400).send({"response": "Tattoo Studio not found"})
            Object.assign(tattooStudio.fields, doc)
            if(!await tattooStudio.save()) return res.sendStatus(422)
            return res.status(200).send({"response": "Tattoo Studio successfully updated"})
        } catch (error) {
            return res.json({
                error
            })
        }
    }

}

export const instance = new TattooStudioController() 