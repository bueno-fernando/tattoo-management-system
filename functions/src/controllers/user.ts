import { Request, Response } from "express"
import { User, UserDocument } from "../models/user"
import * as moment from 'moment-timezone'
import { toMoment } from "../utils/date"

export class UserController{
    
    async route_postJSON(req: Request, res: Response) {
        try {
            const doc = req.body as UserDocument
            const user = new User()
            if (!doc.tattooStudioId) return res.status(400).send({"response": "tattooStudioId is necessary"})
            Object.assign(user.fields, doc)
            if (!await user.create()) return res.sendStatus(422)
            user.fields.id = user.id
            return res.json({
                "user": toMoment(user.fields)
            })
        } catch (error) {
            return res.json({
                error
            })
        }
    }

    async route_getJSON(req: Request, res: Response) {
        const userId = req.params.userId
        const user = new User()
        if (!await user.load(userId)) return res.status(400).send({"response": "user not found", userId: userId})
        user.fields.id = user.id
        return res.json({
            "user": toMoment(user.fields) 
        })
    }

    async route_deleteJSON(req: Request, res: Response) {
        const userId = req.params.userId
        const user = new User()
        if (!await user.delete(userId)) return res.status(400).send({"response": "user not found", userId: userId})
        return res.status(200).send({"response": "user successfully deleted"})
    }

    async route_putJSON(req: Request, res: Response) {
        try {
            const doc = req.body as UserDocument
            const userId = req.params.userId
            const user = new User()
            if(!await user.load(userId)) return res.status(400).send({"response": "user not found"})
            Object.assign(user.fields, doc)
            if(!await user.save()) return res.sendStatus(422)
            return res.status(200).send({"response": "user successfully updated"})
        } catch (error) {
            return res.json({
                error
            })
        }
    }

}

export const instance = new UserController() 