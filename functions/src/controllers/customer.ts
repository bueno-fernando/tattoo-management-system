import { Request, Response } from "express"
import { Customer, CustomerDocument } from "../models/customer"
import * as moment from 'moment-timezone'
import { toMoment } from "../utils/date"

export class CustomerController{
    
    async route_postJSON(req: Request, res: Response) {
        try {
            const doc = req.body as CustomerDocument
            const customer = new Customer()
            Object.assign(customer.fields, doc)
            if (!await customer.create()) return res.sendStatus(422)
            customer.fields.id = customer.id
            customer.fields = toMoment(customer.fields)
            return res.json({
                "customer": customer.fields,
                "now": moment().tz('America/Sao_Paulo').format()
            })
        } catch (error) {
            return res.json({
                error
            })
        }
    }

    async route_getJSON(req: Request, res: Response) {
        const customerId = req.params.customerId
        if (!customerId) return res.status(400).send({"response": "customerId is necessary"})
        const customer = new Customer()
        if (!await customer.load(customerId)) return res.status(400).send({"response": "customer not found", customerId: customerId})
        customer.fields.id = customer.id
        customer.fields = toMoment(customer.fields)
        return res.json({
            "customer": customer.fields, 
            "now": moment().tz('America/Sao_Paulo').format()
        })
    }

    async route_deleteJSON(req: Request, res: Response) {
        const customerId = req.params.customerId
        if (!customerId) return res.status(400).send({"response": "customerId is necessary"})
        const customer = new Customer()
        if (!await customer.delete(customerId)) return res.status(400).send({"response": "customer not found", customerId: customerId})
        return res.status(200).send({"rensponse": "Customer successfully deleted"})
    }

    async route_putJSON(req: Request, res: Response) {
        try {
            const doc = req.body as CustomerDocument
            const customerId = req.params.customerId
            const customer = new Customer()
            if(!await customer.load(customerId)) return res.status(400).send({"response": "customer not found"})
            Object.assign(customer.fields, doc)
            if(!await customer.save()) return res.sendStatus(422)
            return res.status(200).send({"response": "customer successfully updated"})
        } catch (error) {
            return res.json({
                error
            })
        }
    }

}

export const instance = new CustomerController() 