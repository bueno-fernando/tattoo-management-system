import { Request, Response } from "express"
import { Schedule, ScheduleDocument } from "../models/schedule"
import * as moment from 'moment-timezone'
import { toMoment } from "../utils/date"

export class ScheduleController{    

    async route_postJSON(req: Request, res: Response) {
        try {
            const doc = req.body as ScheduleDocument
            const schedule = new Schedule()
            Object.assign(schedule.fields, doc)
            if (!await schedule.create()) return res.sendStatus(422)
            schedule.fields.id = schedule.id
            schedule.fields = toMoment(schedule.fields)
            return res.json({
                "schedule": schedule.fields,
                "now": moment().tz('America/Sao_Paulo').format()
            })
        } catch (error) {
            return res.json({
                error
            })
        }
    }

    async route_getJSON(req: Request, res: Response) {
        const scheduleId = req.params.scheduleId
        if (!scheduleId) return res.status(400).send({"response": "scheduleId is necessary"})
        const schedule = new Schedule()
        if (!await schedule.load(scheduleId)) return res.status(400).send({"response": "schedule not found", scheduleId: scheduleId})
        schedule.fields.id = schedule.id
        schedule.fields = toMoment(schedule.fields)
        return res.json({
            "schedule": schedule.fields, 
            "now": moment().tz('America/Sao_Paulo').format()
        })
    }

    async route_deleteJSON(req: Request, res: Response) {
        const scheduleId = req.params.scheduleId
        if(!scheduleId) return res.status(400).send({"response": "scheduleId is necessary"})
        const schedule = new Schedule()
        if(! await schedule.delete(scheduleId)) return res.status(400).send({"response": "schedule not found", scheduleId: scheduleId})
        return res.status(200).send({"response": "schedule succsefully deleted"})
    }

    async route_putJSON(req: Request, res: Response) {
        try {
            const doc = req.body as ScheduleDocument
            const scheduleId = req.params.scheduleId
            const schedule = new Schedule()
            if(!await schedule.load(scheduleId)) return res.status(400).send({"response": "schedule not found"})
            Object.assign(schedule.fields, doc)
            if(!await schedule.save()) return res.sendStatus(422)
            return res.status(200).send({"response": "schedule successfully updated"})
        } catch (error) {
            return res.json({
                error
            })
        }
    }
}

export const instance = new ScheduleController() 