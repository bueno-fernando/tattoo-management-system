import { Request, Response } from "express"
import { Order, OrderDocument } from "../models/order"
import * as moment from 'moment-timezone'
import { toMoment } from "../utils/date"

export class OrderController{    

    async route_postJSON(req: Request, res: Response) {
        try {
            const doc = req.body as OrderDocument
            const order = new Order()
            if (!doc.customerId || !doc.tattooStudioId) return res.status(400).send({"response": "customerId and tattooStudioId are necessary"})
            Object.assign(order.fields, doc)
            if (!await order.create()) return res.sendStatus(422)
            order.fields.id = order.id
            return res.json({
                "order": toMoment(order.fields)
            })
        } catch (error) {
            return res.json({
                error
            })
        }
    }

    async route_getJSON(req: Request, res: Response) {
        const orderId = req.params.orderId
        const tattooStudioId = req.query.tattooStudioId
        if (!orderId && !tattooStudioId) return res.status(400).send({"response": "orderId or tattooStudioId are necessary"})
        const order = new Order() 
        if (orderId) {
            if (!await order.load(orderId)) return res.status(400).send({"response": "order not found", orderId: orderId})
            order.fields.id = order.id
            return res.json({
                "order": toMoment(order.fields) 
            })
        }
        if (!await order.loadByProperty("tattooStudioId", tattooStudioId)) return res.status(400).send({"response": "Tatto Studio's orders not found", tattooStudioId})
        order.fields.id = order.id
            return res.json({
                "order": toMoment(order.fields) 
            })
    }

    async route_deleteJSON(req: Request, res: Response) {
        const orderId = req.params.orderId
        const order = new Order()
        if(! await order.delete(orderId)) return res.status(400).send({"response": "order not found", orderId: orderId})
        return res.status(200).send({"response": "order succsefully deleted"})
    }

    async route_putJSON(req: Request, res: Response) {
        try {
            const doc = req.body as OrderDocument
            const orderId = req.params.orderId
            const order = new Order()
            if(!await order.load(orderId)) return res.status(400).send({"response": "order not found"})
            Object.assign(order.fields, doc)
            if(!await order.save()) return res.sendStatus(422)
            return res.status(200).send({"response": "Order successfully updated"})
        } catch (error) {
            return res.json({
                error
            })
        }
    }
}

export const instance = new OrderController() 