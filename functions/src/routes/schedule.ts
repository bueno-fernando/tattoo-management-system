import {instance as controller} from '../controllers/schedule'
import * as express from "express"

const router = express.Router()
router.post('/', controller.route_postJSON.bind(controller))
router.get('/:scheduleId', controller.route_getJSON.bind(controller))
router.put('/:scheduleId', controller.route_putJSON.bind(controller))
router.delete('/:scheduleId', controller.route_deleteJSON.bind(controller))

export const scheduleRouter = router