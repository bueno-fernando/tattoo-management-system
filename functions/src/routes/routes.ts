import * as express from "express";
import { tattooStudioRouter } from "./tattooStudio"
import { userRouter } from "./user"
import { customerRouter } from "./customer"
import { orderRouter } from "./order"
import { scheduleRouter } from "./schedule"

const app = express()

app.use('/tattooStudio', tattooStudioRouter)
app.use('/user', userRouter)
app.use('/customer', customerRouter)
app.use('/order', orderRouter)
app.use('/schedule', scheduleRouter)

export const routes = app