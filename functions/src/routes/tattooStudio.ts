import {instance as controller} from '../controllers/tattooStudio'
import * as express from "express"

const router = express.Router()
router.post('/', controller.route_postJSON.bind(controller))
router.get('/:tattooStudioId', controller.route_getJSON.bind(controller))
router.put('/:tattooStudioId', controller.route_putJSON.bind(controller))
router.delete('/:tattooStudioId', controller.route_deleteJSON.bind(controller))

export const tattooStudioRouter = router