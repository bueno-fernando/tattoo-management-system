import {instance as controller} from '../controllers/customer'
import * as express from "express"

const router = express.Router()
router.post('/', controller.route_postJSON.bind(controller))
router.get('/:customerId', controller.route_getJSON.bind(controller))
router.put('/:customerId', controller.route_putJSON.bind(controller))
router.delete('/:customerId', controller.route_deleteJSON.bind(controller))

export const customerRouter = router