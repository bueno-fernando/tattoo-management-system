import {instance as controller} from '../controllers/order'
import * as express from "express"

const router = express.Router()
router.post('/', controller.route_postJSON.bind(controller))
router.get('/:?orderId', controller.route_getJSON.bind(controller))
router.put('/:orderId', controller.route_putJSON.bind(controller))
router.delete('/:orderId', controller.route_deleteJSON.bind(controller))

export const orderRouter = router